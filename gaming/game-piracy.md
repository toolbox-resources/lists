# Game Piracy

## Quick Links

- [DDL Sites](#ddl-sites)

- [Repack Sites](#repack-sites)

- [Torrent Sites](#torrent-sites)

# DDL Sites
Can be downloaded directly in the browser or with a download manager
- [**Free Media Heck Yeah's Game Site List**](https://www.reddit.com/r/FREEMEDIAHECKYEAH/wiki/games#wiki.25BAdownloadgames)
- [**CrackHub**](https://crackhub.site) - Direct download FitGirl Repacks
- [**MyAbandonWare**](https://myabandonware.com) - Games from 1978 - 2010
    
- [**Online Fix**](https://online-fix.me) - Online Game Cracks
    
- [**CS.RIN.RU**](https://cs.rin.ru) - Forum
    
- [**SteamUnlocked**](https://steamunlocked.net) - Pre Installed Games
    
- [**AllGamesForYou**](https://agfy.co) - Reuploads games from other sites on this list.
    
- [**CroHasIt**](https://crohasit.net) - The Old SteamUnlocked
    
- [**Classic PC Games**](https://archive.org/details/classicpcgames) - Old Free PC Games Hosted on the Internet Archive
    
- [**GOG-Games**](https://gog-games.com) - Untouched DRM Free Games
    
- [**DOS Game Archive**](https://dosgamesarchive.com)
    
- [**GamesDrive**](https://gamesdrive.net)
    
- [**SCNLOG.ME**](https://scnlog.me/games) -  Links to the games section
    
- [**SteamRIP**](https://steamrip.com)
    
- [**Free Games Hub**](https://freegameshub.co) - Uses AllGamesForYou
    
- [**A Broke Gamer**](https://abrokegamer.com)
    
- [**Games 4 U**](https://g4u.to) - Password is '404'
    
- [**G Load**](https://gload.to)
    
- [**Free To Game**](https://freetogame.com/games) - Already free games
    
- [**Torrminatorr**](https://forum.torrminatorr.com) - Forum
    
- [**DownloadHa**](https://downloadha.com/category/%d8%a8%d8%a7%d8%b2%db%8c-%da%a9%d8%a7%d9%85%d9%be%db%8c%d9%88%d8%aa%d8%b1-pc-computer-game/) - Links to games section

- [**Kaos Krew**](https://kaoskrew.org) - Forum
    
- [**RLSBB**](https://rlsbb.ru)
    
- [**OldGamesDownload**](https://oldgamesdownload.com)
    
- [**AimHaven**](https://aimhaven.com)
    
- [**Skidrow Reloaded**](https://skidrowreloaded.com) - Generally disliked for having a scene group name. Use at your own risk
    
- [**FlashTro**](https://flashtro.com) - Retro Games


# Repack Sites
Highly Compressed Game Downloads, better for people with slower / limited internet connections

- [**FitGirl Repacks**](http://fitgirl-repacks.site) ✓

- [**Masquerade Repacks**](https://masquerade.site) - [Discord](https://discord.com/invite/HP5sQ6c) ✓
    
- [**DODI Repacks**](http://dodi-repacks.site/) ✓
    
- [**Scooter Repacks**](https://scooter-repacks.site)
    
- [**M4CKD0GE Repacks**](https://m4ckd0ge-repacks.me)
    
- [**KaosKrew**](https://kaoskrew.org)
    
- [**ElAmigos**](https://elamigos.site/)
    
- [**Chovka Repacks**](https://repack.info)
    
- [**Gnarly Repacks**](https://gnarly-repacks.site) - [**Discord**](https://discord.com/invite/yksnVtK6)
    
- [**Xatab**](https://m.byxatab.com)
    
- [**CPG Repacks**](https://cpgrepacks.site)
    
- [**Kaptial Sin** - Forum](https://www.kapitalsin.com/forum/)
    
- [**Le Fishe Repacks Discord**](https://discord.gg/W3MfGDXhAS)
    
- [**LiGHT Repacks**](https://drive.google.com/drive/folders/1q4lRUnwVhMQuakl1yM8OH1Je992LpXE)


# Torrent Sites
Use a VPN and a torrent client like qBittorrent.

- [**1337x**](https://1337x.to)

- [**RARGB**](https://rargb.to)
    
- [RuTracker](http://rutracker.org)
    
- [**TorrentGalaxy**](https://torrentgalaxy.to)
    
- [**Cloud Torrents**](https://cloudtorrents.com)
    
- [**FitGirl Repacks**](http://fitgirl-repacks.site)
    
- [**AllTorrents**](https://alltorrents.co)
    
- [**Lime Torrents**](https://limetorrents.pro)


# ⛔DO NOT USE⛔

- **The Pirate Bay** - Filled with malware, you can find anything there on the safer sites listed here.

- **BBRepacks** - Impersonates Black Box Repacks
    
- **CorePack** - One of their admins added malware. [Here you can read their official statement](https://old.reddit.com/r/CrackWatch/comments/8wuyyk/oursincereapologiestoeveryonecorepack/)
    
- **noSTEAM**
    
- **Ocean Of Games**
    
- Sites using Scene Group names like CODEX, SKIDROW, RELOADED, PLAZA, CPY, etc shouldn't be used. Although, skidrowreloaded.com is an exception as it's safe to use.
