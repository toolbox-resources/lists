# iOS Apps
Collection of good iOS apps, free and paid. 

# Other Category Apps
**iOS Apps that are located in other categories**
- [**Health Category**](https://gitlab.com/toolbox-resources/toolbox/-/blob/main/health.md#health) - CTRL + F and search for "ios"
- [**Password Managers / Authenticator Apps**](https://gitlab.com/toolbox-resources/toolbox/-/blob/main/privacy-security/passwords.md#cloud-based-password-managers)


# Apps

- [**Days Since**](https://apps.apple.com/us/app/days-since-sober-day-tracker/id1445348921) - Track how many days / minutes / hours / months / ... since it's been since you did or didn't do something
- [**Chuck**](https://chuck.email) - Email inbox cleaner
