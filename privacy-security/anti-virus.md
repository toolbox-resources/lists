# Anti Virus


# Anti Viruses
Windows Defender is included and is a good anti virus, for people who need extra protection, I would pick one of these anti virues

- [**Kaspersky**](https://kaspersky.ca)
- [**Bitdefender**](https://bitdefender.com)
- [**MalwareBytes**](https://malwarebytes.com)

# Online File Scanners
I would suggest not uploading sensitive files that you want to keep private to these services
- [**VirusTotal**](https://virustotal.com)

# Other Tools Related to Anti Viruses

- [**MalwareBytes Free Adware Cleaner](https://malwarebytes.com/adwcleaner)
- [**DangerZone**](https://dangerzone.rocks) - Makes dangerous PDFs into safe PDFs
- [**Reveal QR**](https://revealqr.app) - Reveal QR codes
