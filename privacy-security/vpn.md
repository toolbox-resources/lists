# VPN

Free and Paid VPNs, VPN tools, and more.

**This list is not in any kind of order.**

# Paid VPNs


- [**Mullvad**](https://mullvad.net)
- [**Surfshark**](https://surfshark.com)
- [**Windscribe**](https://windscribe.com)
- [**ProtonVPN**](https://protonvpn.com)
- [**PrivateVPN**](https://privatevpn.com)
- [**VyprVPN**](https://vyprvpn.com)
- [**NordVPN**](https://nordvpn.com)
- [**Torguard**](https://torguard.net)
- [**Private Internet Access**](https:/privateinternetaccess.com/)
- [**iVPN**](https://ivpn.net)
- [**AdGuardVPN**](https://adguard-vpn.com/en/welcome.html)
- [**oVPN**](https://ovpn.com/en)
- [**AirVPN**](https://airvpn.org)
- [**HideMe**](https://hide.me/en/)


# Free VPNs
_Note: Free VPNs are slower and less private than paid ones. If you want to torrent something, or want a VPN for privacy, I strongly suggest paying for one_

- [**Windscribe** - 10GB / Month](https://windscribe.com/features/use-for-free)
- [**ProtonVPN** - Unlimited Data](https://protonvpn.com/pricing)
- [**AdGuard VPN** - 3GB / Month](https://adguard-vpn.com/en/welcome.html)
- [**Hide.ME** - 10 GB / Month | P2P](https://hide.me/en/free-vpn)
- [**Hotspot Shield** - 500 MB / Day](https://hotspotshield.com/free-vpn)

# IP / DNS Leak Checkers
_Use these to check for IP / DNS leaks when connected to your VPN_

- [**DNS Leak Test by iVPN**](https://www.dnsleaktest.com)
- [**IP Leak**](https://ipleak.net)
- [**DNS Leak**](https://bash.ws/dnsleak)
- [**Browser Leaks** - Checks for DNS, IP, WebRTC Leaks and more.](https://browserleaks.com)
- [**Do I Leak?**](https://www.top10vpn.com/tools/do-i-leak)
- [**What Is My IP Address?**](https://whatismyipaddress.com)

# VPNs you should avoid

- [**HolaVPN** - Peer to peer VPN that is not private at all.](https://hola.org/)
- [**PureVPN** - Handed over logs to the FBI.](https://purevpn.com)
